#!/usr/bin/env python
# encoding: utf-8
import argparse
import os
import shutil
import subprocess
import sys

import yaml

DESCRIPTION = """
Generate Graphviz dot files for all frameworks.
"""

fnull = open("/dev/null", "w")

def generate_dot(fw_dir, build_dir):
    fw_name = os.path.basename(fw_dir)
    ret = subprocess.call(["cmake", fw_dir, "--graphviz={}.dot".format(fw_name)],
        stdout=fnull,
        cwd=build_dir)
    if ret != 0:
        sys.stdout.write("Generating the dot file for {} failed.\n".format(fw_name))
        sys.exit(ret)

def main():
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument("fw_base_dir",
        help="Base dir containing all frameworks")
    parser.add_argument("dot_base_dir",
        help="Destination dir where dot files will be generated")

    args = parser.parse_args()
    fw_base_dir = os.path.abspath(args.fw_base_dir)
    dot_base_dir = os.path.abspath(args.dot_base_dir)

    lst = os.listdir(fw_base_dir)
    for idx, fw_name in enumerate(lst):
        fw_dir = os.path.join(fw_base_dir, fw_name)
        yaml_path = os.path.join(fw_dir, fw_name + ".yaml")
        if not os.path.exists(yaml_path):
            continue

        progress = 100 * (idx + 1) / len(lst)
        print("{}% {}".format(progress, fw_name))

        with open(yaml_path) as f:
            fw_info = yaml.load(f)
            tier = fw_info["tier"]

        build_dir = os.path.join(dot_base_dir, "tier" + str(tier), fw_name)
        if not os.path.exists(build_dir):
            os.makedirs(build_dir)
        generate_dot(fw_dir, build_dir)
        shutil.copy(yaml_path, build_dir)

if __name__ == "__main__":
    sys.exit(main())
